import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MontagemComponent } from './montagem/montagem.component';
import {LoginComponent} from "./login/login.component";
import {AuthGuard} from "./guards/auth.guard";
import {MontagemListComponent} from "./montagem/montagem-list/montagem-list.component";

const routes: Routes = [
  {
    path: '',pathMatch: 'full', redirectTo: 'meus-pcs'
  },
  {
    path: 'monte-seu-pc',
    component: MontagemComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'meus-pcs',
    component: MontagemListComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'login',
    component: LoginComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
