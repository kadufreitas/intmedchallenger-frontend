import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MontagemComponent } from './montagem/montagem.component';
import {HttpClientModule} from "@angular/common/http";
import { ClientComponent } from './client/client.component';
import { LoginComponent } from './login/login.component';
import {AuthService} from "./login/auth.service";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {AuthGuard} from "./guards/auth.guard";
import {TokenService} from "./montagem/guards/token.service";
import { MontagemListComponent } from './montagem/montagem-list/montagem-list.component'

@NgModule({
  declarations: [
    AppComponent,
    MontagemComponent,
    ClientComponent,
    LoginComponent,
    MontagemListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [AuthService, AuthGuard, TokenService],
  bootstrap: [AppComponent]
})
export class AppModule { }
