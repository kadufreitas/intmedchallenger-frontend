import {Component, OnInit} from '@angular/core';
import {AuthService} from "./auth.service";
import {Client_login} from "./client_login";
import {Client_register} from "./client_register";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  isClient = true;
  private client: Client_login = new Client_login();
  private client_register: Client_register = new Client_register();

  constructor(private authService: AuthService) { }

  ngOnInit() {
  }

  login() {
    this.authService.login(this.client)
  }

  createAcount(){
    this.authService.register(this.client_register).subscribe(success => {
      this.isClient= true
      alert('Cadastro feito com sucesso.')
    },(response: {error: Array<string> }) => {
      console.log(response);
      const erros = Object.keys(response.error).reduce((acc,current)=> {
        return acc + response.error[current] + ' '
      },'')
      alert(erros);
    }
  )
  }

  changeForm(){
    this.isClient = !this.isClient;
  }

}
