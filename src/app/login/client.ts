export class Client {
  email: string;
  token: string;
  user_id: number;
  user_name: string;
}
