import {EventEmitter, Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Client_login} from "./client_login";
import {environment} from "../../environments/environment";
import {Router} from "@angular/router";
import {retry} from "rxjs/operators";
import {TokenService} from "../montagem/guards/token.service";
import {Client} from "./client";
import {Client_register} from "./client_register";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private readonly API = `${environment.API}`;

  showMenu = new EventEmitter<boolean>();
  private isAuthenticated: boolean = false;

  constructor(private http: HttpClient, private router: Router, private token: TokenService) { }

  login(client: Client_login) {
    this.http.post(`${this.API}api-token-auth/`, client).subscribe(
      (response: Client) => {
        localStorage.setItem('client', JSON.stringify(response));
        this.token.setToken(response)
        this.isAuthenticated = true;
        this.showMenu.emit(true);
        this.router.navigate(['/']);
      },
      err => {
        this.isAuthenticated = false;
        this.showMenu.emit(false);
        console.error('Error: ' + err)
      }
    )
  }

  register(client: Client_register) {
    return this.http.post(`${this.API}rest-auth/registration/`, client)
  }

  clientIsAuthenticated() {
    let client = JSON.parse(localStorage.getItem('client'));
    if (client){
      this.showMenu.emit(true);
      this.isAuthenticated = true;
    }
    return this.isAuthenticated;
  }
}
