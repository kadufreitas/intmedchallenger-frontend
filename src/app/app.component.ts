import {Component, OnInit} from '@angular/core';
import {AuthService} from "./login/auth.service";
import {Router} from "@angular/router";
import {Client} from "./login/client";
import {TokenService} from "./montagem/guards/token.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'intmedchallenger-frontend';
  showMenu: boolean = false;
  client: Client;
  constructor(private authService: AuthService, private router: Router, private tokenService: TokenService){}

  ngOnInit() {
    this.authService.showMenu.subscribe(
      response => {
        this.client = this.tokenService.getClient();
        this.showMenu = response
      }
    )
  }

  logout() {
    this.showMenu= false
    localStorage.removeItem('client');
    this.router.navigate(['/login'])
  }
}
