import {Injectable} from "@angular/core";
import {Client} from "../../login/client";
const client = JSON.parse(localStorage.getItem('client')) || {};
@Injectable()
export class TokenService{
  private client: Client;

  getClient(): Client{
    if (!this.client){
      this.client = client
    }
    return this.client
  }

  setToken(client){
    this.client = client
  }

}
