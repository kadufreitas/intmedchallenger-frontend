import {Injectable, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import { HttpHeaders } from '@angular/common/http';
import {environment} from "../../environments/environment";
import {TokenService} from "./guards/token.service";
import {Montagem} from "./montagem";

@Injectable({
  providedIn: 'root'
})
export class MontagemService implements OnInit{
  private readonly API = `${environment.API}`;
  private httpOptions: object;
  components: Montagem = {
    processor: [],
    motherBoard: [],
    memoryRAM: [],
    videoBoard: []
  };
  constructor(private http: HttpClient, private token: TokenService) {
    console.log('httpOp', this.httpOptions)
  }

  ngOnInit(){
    console.log('httpOp', this.httpOptions)
  }

  setHttpOptions(){
    this.httpOptions = {
      headers: new HttpHeaders({
        // "content-type": "application/json",
        // "cache-control": "no-cache",
        'Authorization': 'Token ' + this.token.getClient().token
      })
    };
  }

  list () {
    this.setHttpOptions()
    return this.http.get(`${this.API}montagem_list/`, this.httpOptions)
  }

  async loadComponents() {
    this.setHttpOptions()
    await this.http.get(`${this.API}processador/`).subscribe(res => this.components.processor = res);
    await this.http.get(`${this.API}placa_mae/`).subscribe(res => this.components.motherBoard = res);
    await this.http.get(`${this.API}memoria_ram/`).subscribe((res: Array<object>) => this.components.memoryRAM = res);
    await this.http.get(`${this.API}placa_video/`).subscribe(res => this.components.videoBoard = res);
    return this.components;
  }

  save (montagem) {
    this.setHttpOptions()
    return this.http.post(`${this.API}montagem/`, montagem, this.httpOptions)
  }
}
