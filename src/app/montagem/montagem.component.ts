import {Component, DoCheck, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {MontagemService} from "./montagem.service";
import {ActivatedRoute, Route, Router} from "@angular/router";
import {Montagem} from "./montagem";
import {FormBuilder, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-montagem',
  templateUrl: './montagem.component.html',
  styleUrls: ['./montagem.component.scss']
})
export class MontagemComponent implements OnInit{
  montagem: Montagem;
  form: FormGroup;
  components: Montagem = {
    processor: [],
    motherBoard: [],
    memoryRAM: [],
    videoBoard: []
  };
  current = {
    processor: null,
    motherBoard: null,
    memoryRAM: [],
    videoBoard: null
  }
  constructor(private service: MontagemService, private route: ActivatedRoute, private formBuilder: FormBuilder) { }

  async ngOnInit() {
    this.form = this.formBuilder.group({
      processor: [null],
      motherBoard: [null],
      memoryRAM: [null],
      videoBoard: [null]
    });
    this.components = await this.service.loadComponents();
    console.log(this.form.controls)
  }

  onSubmit() {
    let { processor, motherBoard, memoryRAM, videoBoard } = this.form.value;
    let form = new FormData();
    if(videoBoard){
      form.append("videoBoard", videoBoard);
    }
    form.append("processor", processor);
    form.append("motherBoard", motherBoard);
    memoryRAM.forEach(item => {
      form.append("memoryRAM", item);
    });

    this.service.save(form).subscribe(
      success => {
        this.form.reset();
        Object.keys(this.current).forEach(item => {
          if (item === 'memoryRAM'){
            this.current[item] = []
          }else {
            this.current[item] = null
          }
        });
        console.log(this.form.controls, this.current)
        alert('SALVO COM SUCESSO');
      },
    (response: {error: {
      errors: Array<string>
    }}) => {
        alert(response.error.errors.reduce((acc,current)=> {
          return acc + current + ' '
        },''));
      }
    )
  }

  changeComponent(component){
    let elementValue = this.form.controls[component].value;
    if(elementValue === 'null'){
      this.current[component] = null;
      return
    }
    let index = null;
    if(component === 'memoryRAM'){
      let aux = [];
      elementValue.forEach(item => {
        let result = this.findIndex(component, item);
        if (result !== -1){
          aux.push(result)
        }
      });
      index = aux;
    }else {
      index = this.findIndex(component, elementValue)
    }
    this.current[component] = index;
  }

  findIndex(component, elementValue) {
    return this.components[component].findIndex(item=> {
      return item.id == elementValue
    });
  }


}
