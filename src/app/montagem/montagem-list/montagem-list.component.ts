import { Component, OnInit } from '@angular/core';
import {Montagem} from "../montagem";
import {MontagemService} from "../montagem.service";
import {ActivatedRoute} from "@angular/router";
import {AuthService} from "../../login/auth.service";

@Component({
  selector: 'app-montagem-list',
  templateUrl: './montagem-list.component.html',
  styleUrls: ['./montagem-list.component.scss']
})
export class MontagemListComponent implements OnInit {
  montagens: Array<Montagem>;
  constructor(private service: MontagemService, private route: ActivatedRoute, private authService: AuthService)  {}

  ngOnInit() {
    this.list()
  }

  list() {
    console.log('list');
    this.service.list().subscribe(
      (response: Array<Montagem>) => {
        this.montagens = response;
      },
      err => console.log(err)
    )
  }

}
