import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MontagemListComponent } from './montagem-list.component';

describe('MontagemListComponent', () => {
  let component: MontagemListComponent;
  let fixture: ComponentFixture<MontagemListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MontagemListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MontagemListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
